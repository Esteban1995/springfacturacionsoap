package com.htc.billing.ws.endpoint;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.htc.billing.methods.ServiceMethods;
import com.htc.billing.model.BillModel;
import com.htc.billing.model.DetailModel;
import com.htc.billing.service.DetailService;
import com.htc.billing.utils.ExceptionsMessages;
import com.htc.billing.ws.dto.DetailDto;
import com.htc.billing.ws.dto.GetAllBillsRequest;
import com.htc.billing.ws.dto.GetAllBillsResponse;
import com.htc.billing.ws.dto.GetAllDetailsRequest;
import com.htc.billing.ws.dto.GetAllDetailsResponse;
import com.htc.billing.ws.dto.GetAllProductsRequest;
import com.htc.billing.ws.dto.GetAllProductsResponse;
import com.htc.billing.ws.dto.GetBillByIdRequest;
import com.htc.billing.ws.dto.GetBillByIdResponse;
import com.htc.billing.ws.dto.GetDetailByIdBillRequest;
import com.htc.billing.ws.dto.GetDetailByIdBillResponse;
import com.htc.billing.ws.dto.ModifyBillStatusRequest;
import com.htc.billing.ws.dto.ModifyBillStatusResponse;
import com.htc.billing.ws.dto.SaveBillingRequest;
import com.htc.billing.ws.dto.SaveBillingResponse;

@Endpoint
public class FacturacionEndpoint {

	public static final String NAMESPACE_URI = "http://dto.ws.billing.htc.com";
	@Autowired
	private ServiceMethods servicemethods;
	@Autowired
	private DetailService detailservice;
	public FacturacionEndpoint() {

	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllProductsRequest")
	@ResponsePayload
	public GetAllProductsResponse getAllMovies(@RequestPayload GetAllProductsRequest request) {
		return servicemethods.serviceGetAllProducts();
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllBillsRequest")
	@ResponsePayload
	public GetAllBillsResponse getAllBills(@RequestPayload GetAllBillsRequest request) {
		return servicemethods.serviceGetAllBill();

	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllDetailsRequest")
	@ResponsePayload
	public GetAllDetailsResponse getAllBills(@RequestPayload GetAllDetailsRequest request) {
		return servicemethods.serviceGetAllDetails();
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveBillingRequest")
	@ResponsePayload
	public SaveBillingResponse addBill(@RequestPayload SaveBillingRequest request) throws ExceptionsMessages {
		BillModel bill = new BillModel();
		List<DetailModel> det = new ArrayList<>();
		
		for (DetailDto lista : request.getDetailBillDto()) {
			lista.getIdProduct();
			lista.getQuantity();
			det.add(detailservice.getDetail(lista.getIdProduct(), lista.getQuantity()));
		}
		
		bill.setIdCustomer(request.getIdCustomer());
		bill.setDetail(det);
		return servicemethods.serviceSaveBillin(bill);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getDetailByIdBillRequest")
	@ResponsePayload
	public GetDetailByIdBillResponse getAllBills(@RequestPayload GetDetailByIdBillRequest request) {
		return servicemethods.serviceGetDetailById(request.getIdBill());
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "modifyBillStatusRequest")
	@ResponsePayload
	public ModifyBillStatusResponse updateMovie(@RequestPayload ModifyBillStatusRequest request) throws ExceptionsMessages {
		BillModel bill = new BillModel();
		bill.setIdBill(request.getIdbill());
		bill.setObservation(request.getObservation());
		return servicemethods.serviceModifyBillStatus(bill);
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetBillByIdRequest")
	@ResponsePayload
	public GetBillByIdResponse getAllBillsById(@RequestPayload GetBillByIdRequest request) throws ExceptionsMessages {
		return servicemethods.serviceGetBillById(request.getIdbill());
	}
}
