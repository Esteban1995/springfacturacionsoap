//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.31 a las 09:56:12 PM CST 
//


package com.htc.billing.ws.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DetailsDto complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DetailsDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="iddetail" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idbill" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="id_product" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="quantity" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetailsDto", propOrder = {
    "iddetail",
    "idbill",
    "idProduct",
    "quantity",
    "subtotal"
})
public class DetailsDto {

    protected int iddetail;
    protected int idbill;
    @XmlElement(name = "id_product")
    protected int idProduct;
    protected int quantity;
    protected double subtotal;

    /**
     * Obtiene el valor de la propiedad iddetail.
     * 
     */
    public int getIddetail() {
        return iddetail;
    }

    /**
     * Define el valor de la propiedad iddetail.
     * 
     */
    public void setIddetail(int value) {
        this.iddetail = value;
    }

    /**
     * Obtiene el valor de la propiedad idbill.
     * 
     */
    public int getIdbill() {
        return idbill;
    }

    /**
     * Define el valor de la propiedad idbill.
     * 
     */
    public void setIdbill(int value) {
        this.idbill = value;
    }

    /**
     * Obtiene el valor de la propiedad idProduct.
     * 
     */
    public int getIdProduct() {
        return idProduct;
    }

    /**
     * Define el valor de la propiedad idProduct.
     * 
     */
    public void setIdProduct(int value) {
        this.idProduct = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     */
    public void setQuantity(int value) {
        this.quantity = value;
    }

    /**
     * Obtiene el valor de la propiedad subtotal.
     * 
     */
    public double getSubtotal() {
        return subtotal;
    }

    /**
     * Define el valor de la propiedad subtotal.
     * 
     */
    public void setSubtotal(double value) {
        this.subtotal = value;
    }

}
