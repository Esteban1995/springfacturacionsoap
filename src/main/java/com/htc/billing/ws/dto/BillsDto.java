//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.31 a las 09:56:12 PM CST 
//


package com.htc.billing.ws.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para BillsDto complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BillsDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_bill" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="id_customer" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="iva" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="total" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillsDto", propOrder = {
    "idBill",
    "cDate",
    "idCustomer",
    "subtotal",
    "iva",
    "total"
})
public class BillsDto {

    @XmlElement(name = "id_bill")
    protected int idBill;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected Date cDate;
    @XmlElement(name = "id_customer")
    protected int idCustomer;
    protected double subtotal;
    protected double iva;
    protected double total;

    /**
     * Obtiene el valor de la propiedad idBill.
     * 
     */
    public int getIdBill() {
        return idBill;
    }

    /**
     * Define el valor de la propiedad idBill.
     * 
     */
    public void setIdBill(int value) {
        this.idBill = value;
    }

    /**
     * Obtiene el valor de la propiedad cDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public Date getCDate() {
        return cDate;
    }

    /**
     * Define el valor de la propiedad cDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCDate(Date value) {
        this.cDate = value;
    }

    /**
     * Obtiene el valor de la propiedad idCustomer.
     * 
     */
    public int getIdCustomer() {
        return idCustomer;
    }

    /**
     * Define el valor de la propiedad idCustomer.
     * 
     */
    public void setIdCustomer(int value) {
        this.idCustomer = value;
    }

    /**
     * Obtiene el valor de la propiedad subtotal.
     * 
     */
    public double getSubtotal() {
        return subtotal;
    }

    /**
     * Define el valor de la propiedad subtotal.
     * 
     */
    public void setSubtotal(double value) {
        this.subtotal = value;
    }

    /**
     * Obtiene el valor de la propiedad iva.
     * 
     */
    public double getIva() {
        return iva;
    }

    /**
     * Define el valor de la propiedad iva.
     * 
     */
    public void setIva(double value) {
        this.iva = value;
    }

    /**
     * Obtiene el valor de la propiedad total.
     * 
     */
    public double getTotal() {
        return total;
    }

    /**
     * Define el valor de la propiedad total.
     * 
     */
    public void setTotal(double value) {
        this.total = value;
    }

}
