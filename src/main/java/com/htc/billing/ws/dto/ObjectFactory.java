//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.31 a las 09:56:12 PM CST 
//


package com.htc.billing.ws.dto;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.htc.billing.ws.dto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.htc.billing.ws.dto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBillByIdRequest }
     * 
     */
    public GetBillByIdRequest createGetBillByIdRequest() {
        return new GetBillByIdRequest();
    }

    /**
     * Create an instance of {@link GetAllDetailsRequest }
     * 
     */
    public GetAllDetailsRequest createGetAllDetailsRequest() {
        return new GetAllDetailsRequest();
    }

    /**
     * Create an instance of {@link SaveBillingRequest }
     * 
     */
    public SaveBillingRequest createSaveBillingRequest() {
        return new SaveBillingRequest();
    }

    /**
     * Create an instance of {@link DetailDto }
     * 
     */
    public DetailDto createDetailDto() {
        return new DetailDto();
    }

    /**
     * Create an instance of {@link GetAllBillsResponse }
     * 
     */
    public GetAllBillsResponse createGetAllBillsResponse() {
        return new GetAllBillsResponse();
    }

    /**
     * Create an instance of {@link BillsDto }
     * 
     */
    public BillsDto createBillsDto() {
        return new BillsDto();
    }

    /**
     * Create an instance of {@link GetDetailByIdBillResponse }
     * 
     */
    public GetDetailByIdBillResponse createGetDetailByIdBillResponse() {
        return new GetDetailByIdBillResponse();
    }

    /**
     * Create an instance of {@link DetailsDto }
     * 
     */
    public DetailsDto createDetailsDto() {
        return new DetailsDto();
    }

    /**
     * Create an instance of {@link ServiceStatus }
     * 
     */
    public ServiceStatus createServiceStatus() {
        return new ServiceStatus();
    }

    /**
     * Create an instance of {@link GetBillByIdResponse }
     * 
     */
    public GetBillByIdResponse createGetBillByIdResponse() {
        return new GetBillByIdResponse();
    }

    /**
     * Create an instance of {@link ModifyBillStatusRequest }
     * 
     */
    public ModifyBillStatusRequest createModifyBillStatusRequest() {
        return new ModifyBillStatusRequest();
    }

    /**
     * Create an instance of {@link GetAllBillsRequest }
     * 
     */
    public GetAllBillsRequest createGetAllBillsRequest() {
        return new GetAllBillsRequest();
    }

    /**
     * Create an instance of {@link ModifyBillStatusResponse }
     * 
     */
    public ModifyBillStatusResponse createModifyBillStatusResponse() {
        return new ModifyBillStatusResponse();
    }

    /**
     * Create an instance of {@link GetDetailByIdBillRequest }
     * 
     */
    public GetDetailByIdBillRequest createGetDetailByIdBillRequest() {
        return new GetDetailByIdBillRequest();
    }

    /**
     * Create an instance of {@link GetAllProductsRequest }
     * 
     */
    public GetAllProductsRequest createGetAllProductsRequest() {
        return new GetAllProductsRequest();
    }

    /**
     * Create an instance of {@link GetAllDetailsResponse }
     * 
     */
    public GetAllDetailsResponse createGetAllDetailsResponse() {
        return new GetAllDetailsResponse();
    }

    /**
     * Create an instance of {@link SaveBillingResponse }
     * 
     */
    public SaveBillingResponse createSaveBillingResponse() {
        return new SaveBillingResponse();
    }

    /**
     * Create an instance of {@link GetAllProductsResponse }
     * 
     */
    public GetAllProductsResponse createGetAllProductsResponse() {
        return new GetAllProductsResponse();
    }

    /**
     * Create an instance of {@link ProductsDto }
     * 
     */
    public ProductsDto createProductsDto() {
        return new ProductsDto();
    }

}
