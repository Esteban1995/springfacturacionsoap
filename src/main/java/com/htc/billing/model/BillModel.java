package com.htc.billing.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.htc.billing.model.DetailModel;

public class BillModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer idBill;
	private Date cDate;
	private Integer idCustomer;
	private Double subtotal;
	private Double iva;
	private Double discount;
	private Double total;
	private String observation;
	private Boolean state;
	private List<DetailModel> detail;

	public BillModel() {

	}
	
	public BillModel(Double total, List<DetailModel> detail,String observation, Boolean state) {
		this.total=total;
		this.detail=detail;
		this.observation=observation;
		this.state=state;
	}

	public BillModel(Integer idBill, Date cDate, Integer idCustomer, Double subtotal,
			Double iva, Double discount) {
		this.idBill=idBill;
		this.cDate=cDate;
		this.idCustomer=idCustomer;
		this.subtotal=subtotal;
		this.iva=iva;
		this.discount=discount;
	}

	public Integer getIdBill() {
		return idBill;
	}

	public void setIdBill(Integer idBill) {
		this.idBill = idBill;
	}

	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	public Integer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Integer idCustomer) {
		this.idCustomer = idCustomer;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public List<DetailModel> getDetail() {
		return detail;
	}

	public void setDetail(List<DetailModel> detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return "BillModel [idBill=" + idBill + ", cDate=" + cDate + ", idCustomer=" + idCustomer + ", subtotal="
				+ subtotal + ", iva=" + iva + ", discount=" + discount + ", total=" + total + ", observation="
				+ observation + ", state=" + state + ", detail=" + detail + "]";
	}

}
