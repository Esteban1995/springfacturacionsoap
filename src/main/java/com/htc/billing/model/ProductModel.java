package com.htc.billing.model;

import java.io.Serializable;


public class ProductModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer idProduct;
	private String name;
	private Double price;
	private Integer stock;
	private String cDate;
	
	public ProductModel() {
		
	}
	
	public ProductModel(Integer idProduct, String name, Double price, Integer stock, String cDate) {
		this.idProduct=idProduct;
		this.name=name;
		this.price=price;
		this.stock=stock;
		this.cDate=cDate;
	}

	public Integer getIdproduct() {
		return idProduct;
	}

	public void setIdproduct(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getCdate() {
		return cDate;
	}

	public void setCdate(String cDate) {
		this.cDate = cDate;
	}

	@Override
	public String toString() {
		return  idProduct + "..." + name + ", precio:" + price + ", Stock:" + stock
				+ ", Vence el: " + cDate ;
	}

}
