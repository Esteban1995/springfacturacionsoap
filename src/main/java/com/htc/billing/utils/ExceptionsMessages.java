package com.htc.billing.utils;

public class ExceptionsMessages extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final long code;
	private final String description;

	public ExceptionsMessages(Exception ex) {
		super(ex);
		this.code = Constants.ERROR_NULL_VALUES;
		this.description = Constants.MESSAGE_ERROR_NULL_VALUES;
	}

	public ExceptionsMessages(String description, Exception ex) {
		super(ex);
		this.code = Constants.ERROR_NULL_VALUES;
		this.description = description;
	}

	public ExceptionsMessages(long code, String description, Exception ex) {
		super(ex);
		this.code = code;
		this.description = description;
	}

	public ExceptionsMessages(String description) {
		super();
		this.code = Constants.ERROR_NULL_VALUES;
		this.description = description;
	}

	public ExceptionsMessages(long code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	@Override
	public String getMessage() {
		return toString();
	}

	public long getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Error=").append(code).append(", description=").append(description);
		return builder.toString();
	}
}
