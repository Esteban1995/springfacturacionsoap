package com.htc.billing.utils;

public class NullExceptions extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final long code;
	private final String description;

	public NullExceptions(Exception ex) {
		super(ex);
		this.code = Constants.ERROR_NULL_GETVALUES;
		this.description = Constants.MESSAGE_ERROR_NULL_GETVALUES;
	}

	public NullExceptions(String description, Exception ex) {
		super(ex);
		this.code = Constants.ERROR_NULL_GETVALUES;
		this.description = description;
	}

	public NullExceptions(long code, String description, Exception ex) {
		super(ex);
		this.code = code;
		this.description = description;
	}

	public NullExceptions(String description) {
		super();
		this.code = Constants.ERROR_NULL_GETVALUES;
		this.description = description;
	}

	@Override
	public String getMessage() {
		return toString();
	}

	public long getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Error=").append(code).append(", description=").append(description);
		return builder.toString();
	}
}
