package com.htc.billing.utils;

public class Constants {

	private Constants() {
		
	}
	public static final int ERROR_NULL_VALUES = 400;
	public static final String MESSAGE_ERROR_NULL_VALUES = "No debe dejar campos vacios.";

	public static final int ERROR_NULL_GETVALUES = 402;
	public static final String MESSAGE_ERROR_NULL_GETVALUES = "No se encontro registro.";
}
