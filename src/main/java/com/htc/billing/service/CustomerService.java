package com.htc.billing.service;

import java.util.List;

import com.htc.billing.model.CustomerModel;

public interface CustomerService {

	boolean insert(CustomerModel cus);

	List<CustomerModel> loadAllCustomer();

	void getCustomerById(long cust_id);

	boolean modify(Integer id, CustomerModel cus);

	boolean delete(CustomerModel cus);
}
