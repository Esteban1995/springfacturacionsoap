package com.htc.billing.service;

import java.util.List;

import com.htc.billing.model.ProductModel;

public interface ProductService {

	boolean insert(ProductModel pro);

	List<ProductModel> loadAllProducts();

	void getProductsById(long pro_id);

	boolean modify(Integer id, ProductModel pro);

	boolean modifyStock(Integer id, ProductModel pro);

	boolean delete(ProductModel pro);
}
