package com.htc.billing.service;

import java.util.List;

import com.htc.billing.model.DetailModel;

public interface DetailService {

	List<DetailModel> loadAlldetails();

	List<DetailModel> getDetailById(long cust_id);

	boolean modify(Integer id, DetailModel det);

	boolean delete(DetailModel det);

	public DetailModel getDetail(Integer id, Integer catntidad);

	public DetailModel getDetail(List<Integer> det);
}
