package com.htc.billing.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.htc.billing.dao.BillDao;
import com.htc.billing.dao.DetailDao;
import com.htc.billing.dao.ProductDao;
import com.htc.billing.model.BillModel;
import com.htc.billing.model.DetailModel;
import com.htc.billing.model.ProductModel;
import com.htc.billing.service.BillService;
import com.htc.billing.utils.ExceptionsMessages;

@Component
public class BillServiceImpl implements BillService {
	@Autowired
	BillDao billDao;
	@Autowired
	ProductDao productDao;
	@Autowired
	DetailDao detailDao;
	@Autowired
	private Environment env;
	private static final Logger log = LoggerFactory.getLogger(BillServiceImpl.class);

	@Override
	public boolean insert(BillModel bill) throws ExceptionsMessages {
		Boolean result = Boolean.FALSE;
		try {
			if (bill != null) {
				result = true;
			}
			if (result) {
				Double subtotal = 0.00;
				List<ProductModel> products = new ArrayList<>();
				for (DetailModel d : bill.getDetail()) {
					ProductModel p = productDao.findProductsById(d.getIdProduct());

					if (p == null && bill.getIdCustomer() == null) {
						
						log.error("no se encontró el producto");
						throw new ExceptionsMessages(410, "No se encontro producto");

					} else if (d.getQuantity() < 1) {
						
						throw new ExceptionsMessages(408, "El numero debe de ser mayor a 0");
						
					} else if (p.getStock() >= d.getQuantity()) {
						
						d.setSubtotal(p.getPrice() * d.getQuantity());
						subtotal += d.getSubtotal();
						Integer stock = (p.getStock() - d.getQuantity());
						p.setStock(stock);

						productDao.modifyStock(p.getIdproduct(), p);
						products.add(p);
					} else {
						log.error("no hay suficiente producto");
						throw new ExceptionsMessages(408, "No hay suficiente producto");
					}

				}
				java.util.Date fecha = new java.util.Date();
				java.sql.Date fechasql = new java.sql.Date(fecha.getTime());
				String observ = env.getProperty("observacion");
				Double iva = Double.valueOf(env.getProperty("iva"));
				bill.setcDate(fechasql);
				bill.setIva(subtotal * iva);
				bill.setSubtotal(subtotal);
				bill.setTotal(bill.getIva() + bill.getSubtotal());
				bill.setState(true);
				bill.setObservation(observ);

				Integer llave = billDao.insertAndReturn(bill);

				if (llave == null) {
					log.error("No se pudo realizar la operacion");
					return false;
				}

				productDao.updateBatch(products);
				bill.setIdBill(llave);
				result = detailDao.insertBatch(bill.getDetail(), llave);
			} else {
				log.error("No se puede ingresar la factura.");
				throw new ExceptionsMessages(408, "No se pudo ingresar la factura");
			}
		} catch (ExceptionsMessages e) {
			throw e;
		}

		catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		return result;
	}
	

	@Override
	public List<BillModel> loadAllBill() {
		try {
			return billDao.loadAllBill();
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}

	@Override
	public boolean modifystatus(Integer id, BillModel bill) throws ExceptionsMessages {
		Boolean result = Boolean.FALSE;
		try {
			if (bill.getObservation() != null && id != null) {
				result = true;
			}

			if (result) {

				bill.setState(false);
				billDao.modify(id, bill);

				List<DetailModel> dm = detailDao.finddetailById(id);
				List<DetailModel> details = new ArrayList<>();
				details.addAll(dm);

				for (DetailModel d : details) {
					ProductModel p = productDao.findProductsById(d.getIdProduct());
					if (p == null) {
						log.error("no se encontró el producto");
						return false;
					}
					Integer stock = (p.getStock() + d.getQuantity());
					p.setStock(stock);
					productDao.modifyStock(p.getIdproduct(), p);
				}

			}

		} catch (Exception e) {
			log.error(e.getMessage());
			throw new ExceptionsMessages(e);
		}
		return result;
	}
	

	@Override
	public boolean delete(BillModel bil) {
		billDao.delete(bil);
		return Boolean.TRUE;
	}
	

	@Override
	public Integer insertAndReturn(BillModel bill) {
		try {
			return billDao.insertAndReturn(bill);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	

	@Override
	public BillModel findBillById(long bill_id) throws ExceptionsMessages {
		try {
			BillModel bill = billDao.findBillById(bill_id);

			if (bill == null) {
				throw new ExceptionsMessages(400, "El Id no debe de ir vacio...");
			}
			return bill;
		} catch (ExceptionsMessages e) {
			throw e;
		}

		catch (Exception e) {
			log.error(e.getMessage());
			throw new ExceptionsMessages(e);
		}

	}
	

	@Override
	public List<BillModel> findBillByIdList(long bill_id) throws ExceptionsMessages {
		try {
			List<BillModel> list = billDao.findBillByIdList(bill_id);
			if (list.isEmpty()) {
				throw new ExceptionsMessages(400, "No se encontro registro");
			}
			return list;
		} catch (ExceptionsMessages e) {
			throw e;
		} catch (Exception e) {
			throw new ExceptionsMessages(e);
		}
	}

}
