package com.htc.billing.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htc.billing.dao.DetailDao;
import com.htc.billing.model.DetailModel;
import com.htc.billing.service.DetailService;

@Component
public class DetailServiceImpl implements DetailService {

	@Autowired
	DetailDao detailDao;
	private static final Logger log = LoggerFactory.getLogger(BillServiceImpl.class);

	@Override
	public List<DetailModel> loadAlldetails() {
		try {
			return detailDao.loadAlldetail();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

	@Override
	public List<DetailModel> getDetailById(long cust_id) {
		return detailDao.finddetailById(cust_id);

	}

	@Override
	public boolean modify(Integer id, DetailModel det) {
		try {
			detailDao.modify(id, det);
			return Boolean.TRUE;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean delete(DetailModel det) {
		try {
			detailDao.delete(det);
			return Boolean.TRUE;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public DetailModel getDetail(Integer id, Integer catntidad) {
		DetailModel det = new DetailModel();
		det.setIdproduct(id);
		det.setQuantity(catntidad);
		return det;
	}

	@Override
	public DetailModel getDetail(List<Integer> det) {
		return null;
	}

}
