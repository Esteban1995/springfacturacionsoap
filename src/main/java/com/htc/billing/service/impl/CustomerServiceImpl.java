package com.htc.billing.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.htc.billing.dao.CustomerDao;
import com.htc.billing.model.CustomerModel;
import com.htc.billing.service.CustomerService;

public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDao customerDao;
	private static final Logger log = LoggerFactory.getLogger(BillServiceImpl.class);

	@Override
	public boolean insert(CustomerModel cus) {
		try {
			customerDao.insert(cus);
			return Boolean.TRUE;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public List<CustomerModel> loadAllCustomer() {
		try {
			List<CustomerModel> listCust = customerDao.loadAllCustomer();
			for (CustomerModel cus : listCust) {
				log.error(cus.toString());
			}
			return listCust;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

	@Override
	public void getCustomerById(long cust_id) {
		try {
			customerDao.findCustomerById(cust_id);
		} catch (Exception e) {
			log.error(e.getMessage());
		}

	}

	@Override
	public boolean modify(Integer id, CustomerModel cus) {
		try {
			customerDao.modify(id, cus);
			return Boolean.TRUE;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean delete(CustomerModel cus) {
		try {
			customerDao.delete(cus);
			return Boolean.TRUE;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return Boolean.FALSE;
	}

}
