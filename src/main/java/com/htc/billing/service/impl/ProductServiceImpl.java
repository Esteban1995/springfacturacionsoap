package com.htc.billing.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htc.billing.dao.ProductDao;
import com.htc.billing.model.ProductModel;
import com.htc.billing.service.ProductService;

@Component
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;
	private static final Logger log = LoggerFactory.getLogger(BillServiceImpl.class);

	@Override
	public boolean insert(ProductModel pro) {

		return false;
	}

	@Override
	public List<ProductModel> loadAllProducts() {
		try {
			return productDao.loadAllProducts();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public void getProductsById(long pro_id) {
		try {
			ProductModel det = productDao.findProductsById(pro_id);
			det.getPrice();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public boolean modify(Integer id, ProductModel pro) {
		try {
			productDao.modify(id, pro);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean modifyStock(Integer id, ProductModel pro) {
		try {
			productDao.modifyStock(id, pro);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean delete(ProductModel pro) {
		try {
			productDao.delete(pro);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

}
