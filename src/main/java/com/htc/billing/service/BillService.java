package com.htc.billing.service;

import java.util.List;

import com.htc.billing.model.BillModel;
import com.htc.billing.utils.ExceptionsMessages;

public interface BillService {

	boolean insert(BillModel bil) throws ExceptionsMessages;

	List<BillModel> loadAllBill();

	boolean modifystatus(Integer id, BillModel bil) throws ExceptionsMessages;

	boolean delete(BillModel bil);

	public Integer insertAndReturn(BillModel bill);

	public BillModel findBillById(long bill_id) throws ExceptionsMessages;
	
	List<BillModel> findBillByIdList(long bill_id) throws ExceptionsMessages;
}
