package com.htc.billing.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.billing.methods.ServiceMethods;
import com.htc.billing.model.BillModel;
import com.htc.billing.utils.ExceptionsMessages;
import com.htc.billing.ws.dto.DetailDto;
import com.htc.billing.ws.dto.DetailsDto;
import com.htc.billing.ws.dto.GetAllBillsResponse;
import com.htc.billing.ws.dto.GetBillByIdResponse;
import com.htc.billing.ws.dto.ModifyBillStatusResponse;
import com.htc.billing.ws.dto.ServiceStatus;

@RestController
@RequestMapping("/bill")
public class BillController {

	@Autowired
	private ServiceMethods servicemethods;
	@Autowired
	private Environment env;

	@GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	GetAllBillsResponse allProduct() {
		return servicemethods.serviceGetAllBill();
	}

	// @DeleteMapping("/")
	// ModifyBillStatusResponse modifyBill(@RequestBody BillModel bill) throws
	// ExceptionsMessages {
	// return servicemethods.serviceModifyBillStatus(bill);
	// }

	@DeleteMapping("/")
	ResponseEntity<Object> modifyBill(@RequestBody BillModel bill) throws ExceptionsMessages {
		ServiceStatus message = new ServiceStatus();
		HttpHeaders header = new HttpHeaders();
		try {
			servicemethods.serviceModifyBillStatus(bill);
			int code = Integer.valueOf(env.getProperty("succes_code"));
			String description = env.getProperty("description_message");
			message.setCode(code);
			message.setMessage(description);
			header.add(String.valueOf(message.getCode()), message.getMessage());

		} catch (ExceptionsMessages e) {
			
			message.setCode((int) e.getCode());
			message.setMessage(e.getDescription());
			header.add(String.valueOf(message.getCode()), message.getMessage());
			return new ResponseEntity<>(header, HttpStatus.valueOf(message.getCode()));
			
		} catch (Exception e) {
			message.setCode(400);
			message.setMessage("No se pudo realizar la accion...");
			header.add(String.valueOf(message.getCode()), message.getMessage());
			return new ResponseEntity<>(header, HttpStatus.valueOf(message.getCode()));
		}
		return new ResponseEntity<>(header, HttpStatus.OK);
	}

	// @PostMapping("/")
	// SaveBillingResponse newBill(@RequestBody BillModel bill) throws
	// ExceptionsMessages {
	// return servicemethods.serviceSaveBillin(bill);
	// }

	@PostMapping("/")
	ResponseEntity<Object> newBill(@RequestBody BillModel bill) throws ExceptionsMessages {
		ServiceStatus message = new ServiceStatus();
		HttpHeaders header = new HttpHeaders();
		try {
			servicemethods.serviceSaveBillin(bill);
			int code = Integer.valueOf(env.getProperty("succes_code"));
			String description = env.getProperty("description_message");
			message.setCode(code);
			message.setMessage(description);
			header.add(String.valueOf(message.getCode()), message.getMessage());

		} catch (ExceptionsMessages e) {
			
			message.setCode((int) e.getCode());
			message.setMessage(e.getDescription());
			header.add(String.valueOf(message.getCode()), message.getMessage());
			return new ResponseEntity<>(header, HttpStatus.valueOf(message.getCode()));
			
		} catch (Exception e) {
			
			message.setCode(400);
			message.setMessage("No se pudo realizar la accion...");
			header.add(String.valueOf(message.getCode()), message.getMessage());
			return new ResponseEntity<>(header, HttpStatus.valueOf(message.getCode()));
			
		}
		return new ResponseEntity<>(header, HttpStatus.OK);
	}

	// @GetMapping(path = "/{idbill}", produces=MediaType.APPLICATION_JSON_VALUE)
	// GetBillByIdResponse getBillById(@PathVariable Integer idbill) throws
	// ExceptionsMessages {
	// return servicemethods.serviceGetBillById(idbill);
	// }

	@GetMapping(path = "/{idbill}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Object> allBill(@PathVariable Integer idbill) throws ExceptionsMessages {
		ServiceStatus message = new ServiceStatus();
		GetBillByIdResponse bill = new GetBillByIdResponse();
		HttpHeaders header = new HttpHeaders();
		try {
			bill =  servicemethods.serviceGetBillById(idbill);
			int code = Integer.valueOf(env.getProperty("succes_code"));
			String description = env.getProperty("description_message");
			message.setCode(code);
			message.setMessage(description);
			
			header.add(String.valueOf(message.getCode()), message.getMessage());
			
		} catch (ExceptionsMessages e) {
			
			message.setCode((int) e.getCode());
			message.setMessage(e.getDescription());
			header.add(String.valueOf(message.getCode()), message.getMessage());
			return new ResponseEntity<>(header, HttpStatus.valueOf(message.getCode()));
			
		} catch (Exception e) {
			
			message.setCode(400);
			message.setMessage("No se pudo realizar la accion...");
			header.add(String.valueOf(message.getCode()), message.getMessage());
			return new ResponseEntity<>(header, HttpStatus.valueOf(message.getCode()));
		}
		return new ResponseEntity<>(bill, header, HttpStatus.OK);
		

	}

}
