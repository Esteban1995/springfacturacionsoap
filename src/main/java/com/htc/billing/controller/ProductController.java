package com.htc.billing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.billing.methods.ServiceMethods;
import com.htc.billing.ws.dto.GetAllProductsResponse;

@RestController
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private ServiceMethods servicemethods;
	
	@GetMapping(path = "/", produces=MediaType.APPLICATION_JSON_VALUE)
	GetAllProductsResponse allProduct() {
		return servicemethods.serviceGetAllProducts();
	}


}
