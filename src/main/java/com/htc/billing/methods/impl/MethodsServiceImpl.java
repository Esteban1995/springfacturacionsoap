package com.htc.billing.methods.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htc.billing.methods.ServiceMethods;
import com.htc.billing.model.BillModel;
import com.htc.billing.model.DetailModel;
import com.htc.billing.model.ProductModel;
import com.htc.billing.service.BillService;
import com.htc.billing.service.DetailService;
import com.htc.billing.service.ProductService;
import com.htc.billing.utils.ExceptionsMessages;
import com.htc.billing.ws.dto.BillsDto;
import com.htc.billing.ws.dto.DetailDto;
import com.htc.billing.ws.dto.DetailsDto;
import com.htc.billing.ws.dto.GetAllBillsResponse;
import com.htc.billing.ws.dto.GetAllDetailsResponse;
import com.htc.billing.ws.dto.GetAllProductsResponse;
import com.htc.billing.ws.dto.GetBillByIdResponse;
import com.htc.billing.ws.dto.GetDetailByIdBillResponse;
import com.htc.billing.ws.dto.ModifyBillStatusResponse;
import com.htc.billing.ws.dto.ProductsDto;
import com.htc.billing.ws.dto.SaveBillingResponse;
import com.htc.billing.ws.dto.ServiceStatus;

@Component
public class MethodsServiceImpl implements ServiceMethods {

	@Autowired
	private ProductService service;
	@Autowired
	private BillService billservice;
	@Autowired
	private DetailService detailservice;

	public GetAllProductsResponse serviceGetAllProducts() {
		GetAllProductsResponse response = new GetAllProductsResponse();
		List<ProductModel> productModelList = service.loadAllProducts();
		List<ProductsDto> allProductsList = new ArrayList<ProductsDto>();
		for (ProductModel model : productModelList) {
			ProductsDto product = new ProductsDto();
			BeanUtils.copyProperties(model, product);
			allProductsList.add(product);
		}
		response.getProductsDto().addAll(allProductsList);
		return response;

	}

	public GetAllBillsResponse serviceGetAllBill() {
		GetAllBillsResponse response = new GetAllBillsResponse();
		List<BillModel> billModelList = billservice.loadAllBill();
		List<BillsDto> allBillsList = new ArrayList<BillsDto>();

		for (BillModel model : billModelList) {
			BillsDto bill = new BillsDto();
			BeanUtils.copyProperties(model, bill);
			allBillsList.add(bill);
		}
		response.getBillsDto().addAll(allBillsList);

		return response;
	}

	public GetAllDetailsResponse serviceGetAllDetails() {
		GetAllDetailsResponse response = new GetAllDetailsResponse();
		List<DetailModel> detailModelList = detailservice.loadAlldetails();
		List<DetailsDto> allDetailsList = new ArrayList<DetailsDto>();
		for (DetailModel model : detailModelList) {
			DetailsDto detail = new DetailsDto();
			BeanUtils.copyProperties(model, detail);
			allDetailsList.add(detail);
		}
		response.getDetailsDto().addAll(allDetailsList);

		return response;
	}

	public SaveBillingResponse serviceSaveBillin(BillModel bill) throws ExceptionsMessages{
		SaveBillingResponse response = new SaveBillingResponse();
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			billservice.insert(bill);
			serviceStatus.setCode(200);
			serviceStatus.setMessage("Content Added Successfully");

		} catch (ExceptionsMessages e) {
//			serviceStatus.setCode((int) e.getCode());
//			serviceStatus.setMessage(e.getDescription());
//			response.setServiveStatus(serviceStatus);
//			return response;
			throw e;
		}catch(Exception e) {
			throw new ExceptionsMessages(e);
		}
		response.setServiveStatus(serviceStatus);
		return response;
	}

	public GetDetailByIdBillResponse serviceGetDetailById(Integer id) {
		GetDetailByIdBillResponse response = new GetDetailByIdBillResponse();

		List<DetailModel> detailModelList = detailservice.getDetailById(id);
		List<DetailsDto> allDetailsList = new ArrayList<DetailsDto>();
		for (DetailModel model : detailModelList) {
			DetailsDto detail = new DetailsDto();
			BeanUtils.copyProperties(model, detail);
			allDetailsList.add(detail);
		}
		response.getDetailsDto().addAll(allDetailsList);

		return response;
	}

	public ModifyBillStatusResponse serviceModifyBillStatus(BillModel bill)throws ExceptionsMessages {
		ModifyBillStatusResponse response = new ModifyBillStatusResponse();
		ServiceStatus serviceStatus = new ServiceStatus();
		// 1. Find if movie available
		try {
			List<BillModel> list = new ArrayList<>();
			list.add(bill);
			for (BillModel lista : list) {
				BillModel model = billservice.findBillById(lista.getIdBill());
				
				if (model.getState() == false) {
					throw new ExceptionsMessages(408, "La factura ya se encuentra anulada");

				} else if (lista.getObservation() == null) {
					throw new ExceptionsMessages(400, "Ingrese la observacion");

				} else {
					BillModel bil = new BillModel();
					bil.setObservation(lista.getObservation());
					boolean flag = billservice.modifystatus(lista.getIdBill(), bil);

					if (flag == false) {
						throw new ExceptionsMessages(408, "No se pudo realizar la operacion");
					} else {
						serviceStatus.setCode(200);
						serviceStatus.setMessage("Content updated Successfully");
					}
				}
			}

		} catch (ExceptionsMessages e) {
//			ServiceStatus error = new ServiceStatus();
//			error.setCode((int) e.getCode());
//			error.setMessage(e.getDescription());
//			response.setServiceStatus(error);
//			return response;
			throw e;
		}catch(Exception e) {
			throw new ExceptionsMessages(e);
		}
		response.setServiceStatus(serviceStatus);
		return response;
	}

	@Override
	public GetBillByIdResponse serviceGetBillById(Integer id) throws ExceptionsMessages {
		GetBillByIdResponse response = new GetBillByIdResponse();
		try {
			List<BillModel> detailModelList = billservice.findBillByIdList(id);
			List<BillsDto> allDetailsList = new ArrayList<BillsDto>();
			for (BillModel model : detailModelList) {
				BillsDto detail = new BillsDto();
				BeanUtils.copyProperties(model, detail);
				allDetailsList.add(detail);
			}

			List<DetailModel> detail = detailservice.getDetailById(id);
			List<DetailDto> allDetail = new ArrayList<DetailDto>();
			for (DetailModel model : detail) {
				DetailDto detail2 = new DetailDto();
				BeanUtils.copyProperties(model, detail2);
				allDetail.add(detail2);
			}
			response.getBillsDto().addAll(allDetailsList);
			response.getDetailDto().addAll(allDetail);
			
		} catch (ExceptionsMessages e) {
//			ServiceStatus error = new ServiceStatus();
//			error.setCode((int) e.getCode());
//			error.setMessage(e.getDescription());
//			response.setServiceStatus(error);
//			return response;
			throw e;
		}catch (Exception e) {
			throw new ExceptionsMessages(e);
		}
		return response;
		
	}
}
