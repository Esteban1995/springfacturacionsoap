package com.htc.billing.methods;

import com.htc.billing.model.BillModel;
import com.htc.billing.utils.ExceptionsMessages;
import com.htc.billing.ws.dto.GetAllBillsResponse;
import com.htc.billing.ws.dto.GetAllDetailsResponse;
import com.htc.billing.ws.dto.GetAllProductsResponse;
import com.htc.billing.ws.dto.GetBillByIdResponse;
import com.htc.billing.ws.dto.GetDetailByIdBillResponse;
import com.htc.billing.ws.dto.ModifyBillStatusResponse;
import com.htc.billing.ws.dto.SaveBillingResponse;

public interface ServiceMethods {
	
	public GetAllProductsResponse serviceGetAllProducts();
	
	public GetAllBillsResponse serviceGetAllBill();
	
	public GetAllDetailsResponse serviceGetAllDetails();
	
	public SaveBillingResponse serviceSaveBillin(BillModel bill) throws ExceptionsMessages;
	
	public GetDetailByIdBillResponse serviceGetDetailById(Integer id);
	
	public ModifyBillStatusResponse serviceModifyBillStatus(BillModel bill) throws ExceptionsMessages;
	
	public GetBillByIdResponse serviceGetBillById(Integer id) throws ExceptionsMessages;

}
