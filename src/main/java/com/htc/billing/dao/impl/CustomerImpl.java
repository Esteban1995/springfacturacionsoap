package com.htc.billing.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.htc.billing.dao.CustomerDao;
import com.htc.billing.model.CustomerModel;

public class CustomerImpl extends JdbcDaoSupport implements CustomerDao {

	@Autowired
	DataSource dataSource;
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(CustomerModel cus) {
		String sql = "INSERT INTO customers " + "(id_customer, name, lastname, direccion) VALUES (?, ?, ?, ?)";
		getJdbcTemplate().update(sql,
				new Object[] { cus.getIdCustomer(), cus.getName(), cus.getLastname(), cus.getDireccion() });
		return true;
	}

	@Override
	public List<CustomerModel> loadAllCustomer() {
		String sql = "SELECT * FROM customers";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<CustomerModel> result = new ArrayList<CustomerModel>();
		for (Map<String, Object> row : rows) {
			CustomerModel cus = new CustomerModel();
			cus.setIdCustomer((Integer) row.get("id_customer"));
			cus.setName((String) row.get("name"));
			cus.setLastname((String) row.get("lastname"));
			cus.setDireccion((String) row.get("direccion"));
			result.add(cus);
		}

		return result;

	}

	@Override
	public CustomerModel findCustomerById(long cust_id) {
		String sql = "SELECT * FROM customers WHERE id_customer = ?";
		return (CustomerModel) getJdbcTemplate().queryForObject(sql, new Object[] { cust_id },
				new RowMapper<CustomerModel>() {
					@Override
					public CustomerModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
						CustomerModel cust = new CustomerModel();
						cust.setIdCustomer(rs.getInt("id_customer"));
						cust.setName(rs.getString("name"));
						cust.setLastname(rs.getString("lastname"));
						cust.setDireccion(rs.getString("direccion"));
						return cust;
					}
				});
	}

	@Override
	public boolean modify(Integer id, CustomerModel cus) {
		String sql = "UPDATE customers SET name = ?,  lastname = ?, direccion = ? WHERE id_customer =" + id;
		getJdbcTemplate().update(sql, new Object[] { cus.getName(), cus.getLastname(), cus.getDireccion() });
		return true;
	}

	@Override
	public boolean delete(CustomerModel cus) {
		String sql = "DELETE FROM customers WHERE id_customer = ?";
		getJdbcTemplate().update(sql, new Object[] { cus.getIdCustomer() });
		return true;
	}

}
