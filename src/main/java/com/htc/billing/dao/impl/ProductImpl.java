package com.htc.billing.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.billing.dao.ProductDao;
import com.htc.billing.model.ProductModel;

@Repository
public class ProductImpl extends JdbcDaoSupport implements ProductDao {

	@Autowired
	DataSource dataSource;
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(ProductModel pro) {
		String sql = "INSERT INTO products "
				+ "(id_product, name, price, stock, expiration_date) VALUES (?, ?, ?, ?, ?)";
		getJdbcTemplate().update(sql,
				new Object[] { pro.getIdproduct(), pro.getName(), pro.getPrice(), pro.getStock(), pro.getCdate() });
		return true;
	}

	@Override
	public List<ProductModel> loadAllProducts() {
		String sql = "SELECT * FROM products";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<ProductModel> result = new ArrayList<ProductModel>();
		for (Map<String, Object> row : rows) {
			ProductModel list = new ProductModel();
			list.setIdproduct((Integer) row.get("id_product"));
			list.setName((String) row.get("name"));
			list.setPrice((Double) row.get("price"));
			list.setStock((Integer) row.get("stock"));
			list.setCdate((String) row.get("cdate"));
			result.add(list);
		}

		return result;
	}

	@Override
	public ProductModel findProductsById(long pro_id) {
		String sql = "SELECT * FROM products WHERE id_product= ?";
		List<ProductModel> list = getJdbcTemplate().query(sql, new Object[] { pro_id }, new RowMapper<ProductModel>() {
			@Override
			public ProductModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				ProductModel pro_1 = new ProductModel();
				pro_1.setIdproduct(rs.getInt("id_product"));
				pro_1.setName(rs.getString("name"));
				pro_1.setPrice(rs.getDouble("price"));
				pro_1.setStock(rs.getInt("stock"));
				pro_1.setCdate(rs.getString("cdate"));
				return pro_1;
			}
		});
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public boolean modify(Integer id, ProductModel pro) {

		return false;
	}

	@Override
	public boolean modifyStock(Integer id, ProductModel pro) {
		String sql = "UPDATE products SET stock = ? WHERE id_product =" + id;
		getJdbcTemplate().update(sql, new Object[] { pro.getStock() });
		return true;
	}

	@Override
	public boolean delete(ProductModel pro) {
		String sql = "DELETE FROM products WHERE id_product = ?";
		getJdbcTemplate().update(sql, new Object[] { pro.getIdproduct() });
		return true;
	}

	@Override
	public void updateBatch(List<ProductModel> products) {
		String sql = "UPDATE products SET stock = ? WHERE id_product = ?";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {

				ProductModel pro = products.get(i);
				ps.setInt(1, pro.getStock());
				ps.setInt(2, pro.getIdproduct());

			}

			public int getBatchSize() {
				return products.size();
			}
		});

	}

}
