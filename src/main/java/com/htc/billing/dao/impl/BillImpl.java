package com.htc.billing.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.htc.billing.dao.BillDao;
import com.htc.billing.model.BillModel;

@Repository
public class BillImpl extends JdbcDaoSupport implements BillDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(BillModel bil) {
		String sql = "INSERT INTO bills "
				+ "(id_bill, creation_date, id_customer, subtotal, iva, total, observation, state) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		getJdbcTemplate().update(sql, new Object[] { bil.getIdBill(), bil.getcDate(), bil.getIdCustomer(),
				bil.getSubtotal(), bil.getIva(), bil.getDiscount(), bil.getTotal(), bil.getObservation(), bil.getState()

		});
		return Boolean.TRUE;
	}

	@Override
	public List<BillModel> loadAllBill() {
		String sql = "SELECT * FROM bills";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<BillModel> result = new ArrayList<BillModel>();
		for (Map<String, Object> row : rows) {
			BillModel obj = new BillModel();
			obj.setIdBill((Integer) row.get("id_bill"));
			obj.setcDate((Date) row.get("creation_date"));
			obj.setIdCustomer((Integer) row.get("id_customer"));
			obj.setSubtotal((Double) row.get("subtotal"));
			obj.setIva((Double) row.get("iva"));
			obj.setTotal((Double) row.get("total"));
			obj.setObservation((String) row.get("observation"));
			obj.setState((Boolean) row.get("state"));
			result.add(obj);
		}

		return result;

	}

	@Override
	public Integer insertAndReturn(BillModel bill) {
		final String INSERT_SQL = "INSERT INTO bills "
				+ "(creation_date, id_customer, subtotal, iva, total, observation, state) VALUES (?, ?, ?, ?, ?, ?, ?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(java.sql.Connection arg0) throws SQLException {
				PreparedStatement ps = arg0.prepareStatement(INSERT_SQL, new String[] { "id_bill" });
				ps.setDate(1, (java.sql.Date) bill.getcDate());
				ps.setInt(2, bill.getIdCustomer());
				ps.setDouble(3, bill.getSubtotal());
				ps.setDouble(4, bill.getIva());
				ps.setDouble(5, bill.getTotal());
				ps.setString(6, bill.getObservation());
				ps.setBoolean(7, bill.getState());
				return ps;
			}

		}, keyHolder);
		return (Integer) keyHolder.getKey(); // now contains the generated key

	}

	@Override
	public BillModel findBillById(long bill_id) {
		String sql = "SELECT * FROM bills WHERE id_bill= ?";
		List<BillModel> list = getJdbcTemplate().query(sql, new Object[] { bill_id }, new RowMapper<BillModel>() {
			@Override
			public BillModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				BillModel b = new BillModel();
				b.setIdBill(rs.getInt("id_bill"));
				b.setcDate(rs.getDate("creation_date"));
				b.setIdCustomer(rs.getInt("id_customer"));
				b.setSubtotal(rs.getDouble("subtotal"));
				b.setIva(rs.getDouble("iva"));
				b.setObservation(rs.getString("observation"));
				b.setState(rs.getBoolean("state"));
				return b;
			}
		});
		if (list.isEmpty())
			return null;
		return list.get(0);

	}

	@Override
	public boolean modify(Integer id, BillModel bil) {
		String sql = "UPDATE bills SET observation = ?, state = ? WHERE id_bill =" + id;
		getJdbcTemplate().update(sql, new Object[] { bil.getObservation(), bil.getState() });
		return true;
	}

	@Override
	public boolean delete(BillModel bil) {
		String sql = "DELETE FROM bills WHERE id_bill = ?";
		getJdbcTemplate().update(sql, new Object[] { bil.getIdBill() });
		return true;
	}

	@Override
	public List<BillModel> findBillByIdList(long bill_id) {
		String sql = "SELECT * FROM bills WHERE id_bill =" + bill_id;
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<BillModel> result = new ArrayList<BillModel>();
		for (Map<String, Object> row : rows) {
			BillModel list = new BillModel();
			list.setIdBill((Integer) row.get("id_bill"));
			list.setcDate((Date) row.get("creation_date"));
			list.setIdCustomer((Integer) row.get("id_customer"));
			list.setSubtotal((Double) row.get("subtotal"));
			list.setIva((Double) row.get("iva"));
			list.setTotal((Double) row.get("total"));
			list.setObservation((String) row.get("observation"));
			list.setState((Boolean) row.get("state"));
			result.add(list);
		}
		return result;
	}

	public XMLGregorianCalendar castDate() {
		Date date = new Date();
		XMLGregorianCalendar xmlDate = null;
		GregorianCalendar gc = new GregorianCalendar();

		gc.setTime(date);

		try {
			xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xmlDate;
	}
}
