package com.htc.billing.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.billing.dao.DetailDao;
import com.htc.billing.model.DetailModel;

@Repository
public class DetailImpl extends JdbcDaoSupport implements DetailDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<DetailModel> loadAlldetail() {
		String sql = "SELECT * FROM details";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<DetailModel> result = new ArrayList<DetailModel>();
		for (Map<String, Object> row : rows) {
			DetailModel list = new DetailModel();
			list.setIddetail((Integer) row.get("id_detail"));
			list.setIdbill((Integer) row.get("id_bill"));
			list.setIdproduct((Integer) row.get("id_product"));
			list.setQuantity((Integer) row.get("quantity"));
			list.setSubtotal((Double) row.get("subtotal"));
			result.add(list);
		}

		return result;
	}

	@Override
	public List<DetailModel> finddetailById(long det_id) {
		String sql = "SELECT * FROM details WHERE id_bill ="+ det_id;
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
					
					List<DetailModel> result = new ArrayList<DetailModel>();
					for (Map<String, Object> row : rows) {
						DetailModel list = new DetailModel();
						list.setIddetail((Integer) row.get("id_detail"));
						list.setIdbill((Integer) row.get("id_bill"));
						list.setIdproduct((Integer) row.get("id_product"));
						list.setQuantity((Integer) row.get("quantity"));
						list.setSubtotal((Double) row.get("subtotal"));
						result.add(list);
					}
					return result;
	}

	@Override
	public boolean modify(Integer id, DetailModel det) {
		String sql = "UPDATE details SET id_product = ?, quantity = ?, subtotal = ? WHERE id_detail ="
				+ id;
		getJdbcTemplate().update(sql,
				new Object[] { det.getIdbill(), det.getIdProduct(), det.getQuantity(), det.getSubtotal() });
		return true;
	}

	@Override
	public boolean delete(DetailModel det) {
		String sql = "DELETE FROM details WHERE id_detail = ?";
		getJdbcTemplate().update(sql, new Object[] { det.getIdbill() });
		return true;
	}

	@Override
	public boolean insertBatch(List<DetailModel> detail, Integer llave) {
		String sql = "INSERT INTO details " + "(id_bill, id_product, quantity, subtotal) VALUES (?, ?, ?, ?)";
		Boolean result = false;
		try {
			getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					DetailModel det = detail.get(i);
					ps.setInt(1, llave);
					ps.setInt(2, det.getIdProduct());
					ps.setInt(3, det.getQuantity());
					ps.setDouble(4, det.getSubtotal());
				}

				public int getBatchSize() {
					return detail.size();
				}
			});
			result = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

}
