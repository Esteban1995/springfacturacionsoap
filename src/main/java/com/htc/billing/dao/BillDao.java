package com.htc.billing.dao;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import com.htc.billing.model.BillModel;

public interface BillDao {

	 boolean insert(BillModel bil);
		
		List<BillModel> loadAllBill();
		
		public Integer insertAndReturn(BillModel bill);
		
		BillModel findBillById(long bill_id);

		boolean modify(Integer id, BillModel bil);
		
		boolean delete(BillModel bil);
		
		List<BillModel> findBillByIdList(long bill_id);
		
		public XMLGregorianCalendar castDate();	
}
