package com.htc.billing.dao;

import java.util.List;

import com.htc.billing.model.DetailModel;

public interface DetailDao {

	List<DetailModel> loadAlldetail();

	List<DetailModel> finddetailById(long det_id);
	
	boolean modify(Integer id, DetailModel det);
	
	boolean delete(DetailModel det);

	boolean insertBatch(List<DetailModel> detail, Integer llave);
}
