package com.htc.billing.dao;

import java.util.List;

import com.htc.billing.model.ProductModel;

public interface ProductDao {

	 boolean insert(ProductModel pro);
		
		List<ProductModel> loadAllProducts();
		
		ProductModel findProductsById(long pro_id);

	    boolean modify(Integer id, ProductModel pro);
	    
	    boolean modifyStock(Integer id, ProductModel pro);

	    boolean delete(ProductModel pro);

		void updateBatch(List<ProductModel> products);
}
