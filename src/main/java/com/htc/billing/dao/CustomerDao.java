package com.htc.billing.dao;

import java.util.List;

import com.htc.billing.model.CustomerModel;

public interface CustomerDao {

	boolean insert(CustomerModel cus);

	List<CustomerModel> loadAllCustomer();

	CustomerModel findCustomerById(long cust_id);

	boolean modify(Integer id, CustomerModel cus);

	boolean delete(CustomerModel cus);
}
