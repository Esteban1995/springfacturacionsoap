package com.htc.facturacion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.billing.model.ProductModel;
import com.htc.billing.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductTest {

	@Autowired
	ProductService proService;

	@Test
	public void insertVehicle() {
		ProductModel pro = new ProductModel();
		pro.setIdproduct(4);
		pro.setName("Pan dulce");
		pro.setPrice(0.25);
		pro.setStock(25);
		pro.setCdate("09-10-2021");

		assertEquals(true, proService.insert(pro));
	}

	@Test
	public void modifyVehicle() {
		ProductModel pro_1 = new ProductModel();
		pro_1.setName("CocaCola 3lts");
		pro_1.setPrice(2.10);
		pro_1.setStock(34);
		pro_1.setCdate("09-10-2019");

		assertEquals(true, proService.modify(1, pro_1));
	}

	@Test
	public void deleteVehicle() {
		ProductModel pro1 = new ProductModel();
		pro1.setIdproduct(1);

		assertEquals(true, proService.delete(pro1));
	}

	@Test
	public void findVehicle() {

		assertNotNull(proService.loadAllProducts());

	}
}
