package com.htc.facturacion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.billing.dao.CustomerDao;
import com.htc.billing.model.CustomerModel;
import com.htc.billing.service.CustomerService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerTest {
	@Autowired
	CustomerService cusService;
	CustomerDao cusDao;

	@Test
	public void TestInsert() {
		CustomerModel cus = new CustomerModel();
		cus.setIdCustomer(99);
		cus.setName("Esteban");
		cus.setLastname("Hernandez");
		cus.setDireccion("Sonsonate");

		assertEquals(true, cusService.insert(cus));
	}

//	@Test
//	public void TestModify() {
//		CustomerModel cus = new CustomerModel();
//		cus.setName("Gerardo");
//		cus.setLastname("Hernandez");
//		cus.setDireccion("sonsonate");
//
//		assertEquals(true, cusService.modify(1, cus));
//	}
//
//	@Test
//	public void TestDelete() {
//		Customer cus = new Customer();
//		cus.setIdCustomer(1);
//
//		assertEquals(true, cusService.delete(cus));
//	}
//
//	@Test
//	public void findCustomer() {
//
//		assertNotNull(cusService.loadAllCustomer());
//
//	}

}

