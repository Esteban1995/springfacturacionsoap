package com.htc.facturacion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.billing.dao.DetailDao;
import com.htc.billing.model.DetailModel;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DetailTest {

	@Autowired
	DetailDao detService;

	@Test
	public void TestModify() {
		DetailModel det = new DetailModel();
		det.setIdbill(12);
		det.setIdproduct(6);
		det.setQuantity(20);
		det.setSubtotal(250.00);

		assertEquals(true, detService.modify(12, det));
	}

	@Test
	public void TestDelete() {
		DetailModel det = new DetailModel();
		det.setIddetail(2);

		assertEquals(true, detService.delete(det));
	}

	@Test
	public void findDetail() {

		assertNotNull(detService.loadAlldetail());

	}

}
