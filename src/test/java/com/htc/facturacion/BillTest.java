package com.htc.facturacion;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.billing.dao.BillDao;
import com.htc.billing.model.BillModel;
import com.htc.billing.model.DetailModel;
import com.htc.billing.service.BillService;
import com.htc.billing.service.DetailService;
import com.htc.billing.utils.ExceptionsMessages;


@RunWith(SpringRunner.class)
@SpringBootTest
public class BillTest {

	@Autowired
	BillService bilService;
	@Autowired
	BillDao bilDao;
	@Autowired
	DetailService detailService;

	@Test
	public void insert() throws ExceptionsMessages {
		java.util.Date fecha = new java.util.Date();
		java.sql.Date fechasql = new java.sql.Date(fecha.getTime());
		
		List<DetailModel> bil = new ArrayList<>();
		bil.add(detailService.getDetail(3, 2));
		BillModel bil1 = new BillModel();
		bil1.setcDate(fechasql);
		bil1.setIdCustomer(1);
		bil1.setDetail(bil);
		
		assertEquals(true, bilService.insert(bil1));
	}
//	public void TestInsertBill() {
//		Date da = new Date();
//		Bill bill_1 = new Bill();
//		bill_1.setIdBill(5554);
//		bill_1.setCdate(da);
//		bill_1.setIdCustomer(1);
//		bill_1.setSubtotal(00.00);
//		bill_1.setIva(00.00);
//		bill_1.setDiscount(0.00);
//		bill_1.setTotal(00.00);
//
//		assertEquals(true, bilService.insert(bill_1));
//	}
//
//	@Test
//	public void TestModify() {
//		Bill bil = new Bill();
//		bil.setSubtotal(250.00);
//		bil.setIva(45.00);
//		bil.setDiscount(0.00);
//		bil.setTotal(295.00);
//
//		assertEquals(true, bilService.modify(5554, bil));
//	}
//
//	@Test
//	public void TestDelete() {
//		Bill bil = new Bill();
//		bil.setIdBill(555);
//
//		assertEquals(true, bilService.delete(bil));
//	}
//
//	@Test
//	public void findBill() {
//		assertNotNull(bilService.loadAllBill());
//	}

}
